from django.urls import path
from .views import list_shoes, shoe_details

urlpatterns = [
    path('shoes/', list_shoes, name='list_shoes'),
    path('shoes/<int:id>/', shoe_details, name='shoe_details'),
]

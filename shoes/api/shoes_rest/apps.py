from django.apps import AppConfig


# make sure that we install this app in settings!!!!!!
class ShoesApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'shoes_rest'

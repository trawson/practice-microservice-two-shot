from django.db import models
from django.urls import reverse


# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)  # we need href for the path
    closet_name = models.CharField(max_length=100)  # on the drop down we really just want to see the name

    def __str__(self):  # this gives the closet name instead of an object number
        return f"{self.closet_name}"


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    image_url = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("shoe_details", kwargs={
            "id": self.id,
        })  # this will get the id from the shoe detail
    # in relation to the view function


# remember to register these models in the ADMIN!

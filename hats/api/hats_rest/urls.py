from django.urls import path
from .views import hat_list, hat_detail

urlpatterns = [
    path('hats/', hat_list, name='hat_list'),
    path('hats/<int:id>/', hat_detail, name='hat_detail'),
]

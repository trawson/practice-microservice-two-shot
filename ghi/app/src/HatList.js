
import React, { useState, useEffect } from "react";

function HatList() {
    const [hats, setHats] = useState([]);
    const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const deleteHat = async(id) => {
        const url = `http://localhost:8090/api/hats/${id}`;
        const response = await fetch(url, {method: "DELETE"});
        if (response.ok) {
            setHats(hats.filter(hat => hat.id !== id));
        }
    };


    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Hats</h1>
            <table className="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats?.map((hat) => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td>
                                <img src={ hat.image_url } width="130" height="100" alt={`${hat.style} hat`} className="img-fluid"/>
                            </td>
                            <td>{hat.location}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => deleteHat(hat.id)}>Kill it.</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
        </div>
            {hats.length === 0 && (
        <div className="row">
            <div className="col-12 text-center">
                <p className="lead">Loading...</p>
            </div>
        </div>
    )}
    </div>
    );
}

export default HatList;

import React, { useEffect, useState } from "react";

function ShoeForm () {
    const[bins, setBins] = useState([]);
    const[formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        image_url: '',
        bin: '',
    });

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        const response = await fetch (shoesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                image_url: '',
                bin: '',
            });
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="my-5 container">
            <div className="row">
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <h1 className="mb-3">
                        Please Choose A Bin. No Really. Choose it. Thanks.
                    </h1>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.bin} name="bin" id="bin" className="dropdownClasses" required>
                            <option value="">Choose a Bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.id}>
                                        {bin.closet_name}
                                    </option>
                                );
                                })}
                        </select>
                    </div>
                        <p className="mb-3">
                            Shoe Stuff
                        </p>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.manufacturer} required placeholder="manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control"/>
                            <label htmlFor="name">Fabric</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.model_name} required placeholder="model_name" type="text" id="model_name" name="model_name" className="form-control"/>
                            <label htmlFor="style">Style Name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} required placeholder="color" type="text" id="color" name="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.image_url} required placeholder="image_url" type="url" id="image_url" name="image_url" className="form-control"/>
                            <label htmlFor="url">Picture URL</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Create!</button>
                </form>
            </div>
        </div>
    )
};

export default ShoeForm;

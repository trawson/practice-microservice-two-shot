import React, { useState, useEffect } from "react";

function ShoeList() {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    const deleteShoe = async(id) => {
        const url = `http://localhost:8080/api/shoes/${id}`;
        const response = await fetch(url, {method: "DELETE"});
        if (response.ok) {
            setShoes(shoes.filter(shoe => shoe.id !== id));
        }
    };


    return (
        <div className="container my-4">
        <div className="row">
            <div className="col-12">
                <h1 className="text-center mb-4">Shoes</h1>
            <table className="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {shoes?.map((shoe) => {
                    return (
                        <tr key={shoe.href}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>
                                <img src={ shoe.image_url } width="130" height="100" alt={`${shoe.manufacturer} ${shoe.model_name}`} className="img-fluid"/>
                            </td>
                            <td>{shoe.bin}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>Kill it.</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
        </div>
            {shoes.length === 0 && (
        <div className="row">
            <div className="col-12 text-center">
                <p className="lead">Loading...</p>
            </div>
        </div>
    )}
    </div>
    );
}

export default ShoeList;
